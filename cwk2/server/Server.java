import java.util.concurrent.*;
import java.net.*;
import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;

class clientHandler extends Thread{
	static Socket socket;
	static PrintWriter out = null;
	static BufferedReader in = null;
	static InetAddress ip = null;
	static LocalDateTime connectionTime = null;

	public static void handler (String request, int mode) throws IOException { //mode is 0 for get, 1 for put
		byte[] buffer = new byte[1024];
		File serverDirectory = new File("./serverFiles");

		if (request.equals("list")) { //Printing the list of files in serverFiles
			String[] fileList = serverDirectory.list();
			String listOfFiles = "";
			for (int i = 0; i < fileList.length; i++) {
				listOfFiles = listOfFiles.concat(fileList[i] + ", ");
			}
			out.println(listOfFiles);
		} else if (request.equals("get")) { //hacky way of making it read the filename
			handler(in.readLine(), 0);
		} else if (request.equals("put")) {
			handler(in.readLine(), 1);
		} else if (request.equals("log")) { //logging the client's connection to a file
			System.out.println("Client has connected from " + ip.toString() + " at time " + connectionTime.toString());
			File logfile = new File(".", "log.txt");
			if (!logfile.exists()) {
				logfile.createNewFile();
			}
			logfile.setWritable(true);
			byte[] logWrite = (ip.toString() + ", ").getBytes();
			Files.write(logfile.toPath(), logWrite, StandardOpenOption.APPEND);
			logWrite = (connectionTime.toString() + "\n").getBytes();
			Files.write(logfile.toPath(), logWrite, StandardOpenOption.APPEND);
		} else {
			File file = null;
			try {
				if (mode == 0) { //get
					file = new File(serverDirectory, request);
					if (file.canRead()) {
						socket.getOutputStream().write(Files.readAllBytes(file.toPath()));
						socket.getOutputStream().flush();
					} else {
						System.out.println("Cannot read file");
					}
				} else if (mode == 1) { //put
					file = new File(serverDirectory, request);
					if (file.exists()) {
						file.delete();
					}
					file.createNewFile();
					while (socket.getInputStream().read(buffer, 0, 1024) > 0) {
						Files.write(file.toPath(), buffer, StandardOpenOption.APPEND);
					}
				} else { //error handling
					System.out.println("Unknown mode value");
					return;
				}
			} catch (NullPointerException e) { //more error handling
				System.out.println("The file you requested does not exist");
			} catch (IOException e) {
				System.out.println("An IOException occured");
			}
		}
	}

	public void run() {
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String input = null;

			ip = socket.getInetAddress(); //logging
			connectionTime = LocalDateTime.now();
			handler("log", 0);

			while((input = in.readLine()) != null){ //main loop of the program
				System.out.println(input);
				handler(input, 0);
			}
			out.close();
			in.close();
			socket.close();
		} catch (IOException e) {
			System.out.println("An IO error occured");
			return;
		}
	}

	public clientHandler(Socket socket) {
		super("clientHandler");
		this.socket = socket;
	}
}

public class Server {
	static ServerSocket server = null;
	static ExecutorService executor = null;
	static Socket client = null;

	public static void main( String[] args ){
		try {
			server = new ServerSocket(8888); //Initialising the socket as bound to port 8888
		} catch (IOException e) { //In case an I/O error happens when initialising the socket
			System.out.println(e);
			return;
		}
		executor = Executors.newFixedThreadPool(10); //Creating a thread pool of 10 threads using an executor
		System.out.println("The server is running");
		while (true) {
			try {
				client = server.accept();
				executor.submit(new clientHandler(client));
			} catch (IOException e) {
				System.out.println(e);
				return;
			}
		}
	}
}
