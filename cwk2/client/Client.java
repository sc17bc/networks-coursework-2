import java.net.*;
import java.io.*;
import java.nio.file.*;

public class Client {
	public static void main( String[] args ){
		Socket socket = null;
		PrintWriter socketOutput = null;
		BufferedReader socketInput = null;

		String fromUser = null;
		String fromServer = null;
		byte[] buffer = new byte[1024];
		File clientDirectory = new File("./clientFiles");
		File file = null;

		if (args.length == 0) {
			System.out.println("No arguments have been entered");
			return;
		}

		try { //Initialising the socket
			socket = new Socket("localhost", 8888);
			socketOutput = new PrintWriter(socket.getOutputStream(), true);
			socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (UnknownHostException e){
			System.out.println("Host unknown");
			return;
		} catch (IOException e) {
			System.out.println("An IO error occured");
			return;
		}

		try {
			socketOutput.println(args[0]);
			if (args.length > 1) { //get and put
				socketOutput.println(args[1]);
				if (args[0].equals("get")) {
					file = new File(clientDirectory, args[1]);
					if (file.exists()) {
						file.delete();
					}
					file.createNewFile();
					int x = 0;
					byte[] minibuffer = null;
					while ((x = socket.getInputStream().read(buffer, 0, 1024)) > 0) {
						if (x == 1024) {
							Files.write(file.toPath(), buffer, StandardOpenOption.APPEND);
							buffer = new byte[1024];
						} else {
							minibuffer = new byte[x];
							for (int i = 0; i < x; i++) {
								minibuffer[i] = buffer[i];
							}
							Files.write(file.toPath(), minibuffer, StandardOpenOption.APPEND);
							break;
						}
					}
				} else if (args[0].equals("put")) {
					file = new File(clientDirectory, args[1]);
					if (file.canRead()) {
						socket.getOutputStream().write(Files.readAllBytes(file.toPath()));
						socket.getOutputStream().flush();
					} else {
						System.out.println("Cannot read file");
					}
				}
			} else { //list
				System.out.println(socketInput.readLine());
			}
		} catch (IOException e){
			System.out.println(e.toString());
			return;
		}

		try {
			socketOutput.close();
			socketInput.close();
			socket.close();
			return;
		} catch (IOException e){
			System.out.println("An IO error occured");
			return;
		}
	}
}
